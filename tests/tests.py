import unittest
import os
import logging
import sys
import json
import glob

import wnfm.core

log = logging.getLogger("wnfm")
log.setLevel(logging.DEBUG)
formatter = logging.Formatter(fmt='%(message)s')
if sys.version_info[0] == 2 and sys.version_info[1] <= 6:
    fh = logging.StreamHandler(strm=sys.stdout)
else:
    fh = logging.StreamHandler(stream=sys.stdout)
fh.setFormatter(formatter)
log.addHandler(fh)


class WNFmTests(unittest.TestCase):
    def setUp(self):
        self.plugin_dir = 'tests/fixtures/plugins'
        self.out_dir = 'tests/fixtures/out'

    def test_core_json_1p(self):
        plugins = [f for f in glob.glob(self.plugin_dir + "/*")
                   if os.path.isfile(f) and os.access(f, os.X_OK)]

        wnfm.core.wn_fm_pool(self.plugin_dir, self.out_dir, 'jsonify', pool_size=1)

        for plugin in plugins:
            self.assertTrue(os.path.exists(plugin))
            out_file = os.path.splitext(os.path.basename(plugin))[0]+".json"
            out = os.path.join(os.path.abspath(self.out_dir), out_file)
            self.assertTrue(os.path.exists(out))
            os.remove(out)

    def test_core_json_2p(self):
        plugins = [f for f in glob.glob(self.plugin_dir + "/*")
                   if os.path.isfile(f) and os.access(f, os.X_OK)]

        wnfm.core.wn_fm_pool(self.plugin_dir, self.out_dir, 'jsonify', pool_size=2)

        for plugin in plugins:
            self.assertTrue(os.path.exists(plugin))
            out_file = os.path.splitext(os.path.basename(plugin))[0] + ".json"
            out = os.path.join(os.path.abspath(self.out_dir), out_file)
            self.assertTrue(os.path.exists(out))
            os.remove(out)

    def test_core_json_5p(self):
        plugins = [f for f in glob.glob(self.plugin_dir + "/*")
                   if os.path.isfile(f) and os.access(f, os.X_OK)]

        wnfm.core.wn_fm_pool(self.plugin_dir, self.out_dir, 'jsonify', pool_size=5, plugin_timeout=6)

        for plugin in plugins:
            self.assertTrue(os.path.exists(plugin))
            out_file = os.path.splitext(os.path.basename(plugin))[0] + ".json"
            out = os.path.join(os.path.abspath(self.out_dir), out_file)
            self.assertTrue(os.path.exists(out))
            os.remove(out)

    def test_core_dummy_5p_bt(self):
        plugins = [f for f in glob.glob(self.plugin_dir + "/*")
                   if os.path.isfile(f) and os.access(f, os.X_OK)]

        wnfm.core.wn_fm_pool(self.plugin_dir, self.out_dir, 'dummy', pool_size=5, backend_timeout=5)

        for plugin in plugins:
            self.assertTrue(os.path.exists(plugin))
            out_file = os.path.splitext(os.path.basename(plugin))[0] + ".json"
            out = os.path.join(os.path.abspath(self.out_dir), out_file)
            self.assertFalse(os.path.exists(out))

    def test_core_json_5p_pt(self):
        plugins = [f for f in glob.glob(self.plugin_dir + "/*")
                   if os.path.isfile(f) and os.access(f, os.X_OK)]

        wnfm.core.wn_fm_pool(self.plugin_dir, self.out_dir, 'jsonify', pool_size=5)

        for plugin in plugins:
            self.assertTrue(os.path.exists(plugin))
            out_file = os.path.splitext(os.path.basename(plugin))[0] + ".json"
            out = os.path.join(os.path.abspath(self.out_dir), out_file)
            if 'p4' in out_file:
                with open(out, 'r') as f:
                    po = json.load(f)
                    self.assertTrue(po['retcode'] == 255)
            else:
                self.assertTrue(os.path.exists(out))
            os.remove(out)

    # def test_core_json_2p_env(self):
    #     plugins = ['tests/fixtures/plugins/p1.sh', 'tests/fixtures/plugins/p2.sh', 'tests/fixtures/plugins/p5.sh']
    #
    #     os.environ['ETF_TESTS'] = 'tests/fixtures/plugins/p1.sh,tests/fixtures/plugins/p2.sh,' \
    #                               'tests/fixtures/plugins/p5.sh'
    #     wnfm.core.wn_fm_pool(self.plugin_dir, self.out_dir, 'jsonify', pool_size=2, plugin_timeout=6)
    #
    #     for plugin in plugins:
    #         self.assertTrue(os.path.exists(plugin))
    #         out_file = os.path.splitext(os.path.basename(plugin))[0] + ".json"
    #         out = os.path.join(os.path.abspath(self.out_dir), out_file)
    #         if 'p1' in out_file or 'p5' in out_file:
    #             with open(out, 'r') as f:
    #                 po = json.load(f)
    #                 self.assertTrue(po['retcode'] == 3)
    #         else:
    #             self.assertTrue(os.path.exists(out))
    #         os.remove(out)


if __name__ == '__main__':
    unittest.main()
