import logging
import sys
import os
import glob
import datetime
import multiprocessing
import pkgutil
import time
import signal
import pexpect
import socket
import traceback
import io

import wnfm
import wnfm.backends
import wnfm.backends.jsonify
import wnfm.backends.dummy

__all__ = ['status', 'wn_fm_pool']
log = logging.getLogger('wnfm')

BACKENDS_MAP = {'jsonify': wnfm.backends.jsonify,
                'dummy': wnfm.backends.dummy}


def status(ret_code):
    status_map = {0: "OK", 1: "WARNING", 2: "CRITICAL", 3: "UNKNOWN"}
    status_map_same = {10: "OK", 20: "UNKNOWN", 30: "UNKNOWN", 40: "WARNING", 50: "CRITICAL", 60: "CRITICAL", 100: "UNKNOWN"}
    if ret_code in status_map.keys():
        return status_map[ret_code]
    elif ret_code in status_map_same.keys():
        return status_map_same[ret_code]
    else:
        return "UNKNOWN"


class TimeoutError(Exception):
    pass


def _handle_timeout(signum, frame):
    raise TimeoutError("Backend execution timed out")


def mp_worker(plugin):
    out_dir = config['output_dir']
    plugin_timeout = config['plugin_timeout']
    backend = config['backend']
    backend_timeout = config['backend_timeout']
    static_backend = config['static_backend']
    plugin_tmp_file = os.path.join(out_dir, 'tmp_'+os.path.basename(plugin))
    plugin_summary = ''
    ret_code = 3

    # Plugin
    log.debug("Worker %d started" % os.getpid())
    if sys.version_info[0] == 3:
        tmp_fo = open(plugin_tmp_file, 'w', encoding='utf-8', errors='ignore')
    elif sys.version_info[0] == 2 and sys.version_info[1] == 7:
        tmp_fo = io.open(plugin_tmp_file, 'w', encoding='utf-8', errors='ignore')
    else:
        tmp_fo = open(plugin_tmp_file, 'w')

    # tmp_fo = os.fdopen(os.open(plugin_tmp_file, os.O_RDWR | os.O_CREAT), 'w')
    # tmp_fo = io.open(plugin_tmp_file, 'w', encoding='utf-8', errors='ignore')
    try:
        if sys.version_info[0] == 2 and sys.version_info[1] <= 6:
            child = pexpect.spawn(plugin, timeout=plugin_timeout, logfile=tmp_fo,
                                  env=os.environ)
        else:
            child = pexpect.spawn(plugin, timeout=plugin_timeout, logfile=tmp_fo,
                                  env=os.environ, encoding='utf-8')
    except Exception as e:
        log.exception("Exception was caught while spawning the test (%s)" % str(e))
        return
    pp_id = child.pid

    try:
        log.debug("    Subprocess %s (%d) started" % (plugin, pp_id))
        child.expect(None)
    except pexpect.EOF:
        child.close(force=True)
        ret_code = child.exitstatus
        if child.signalstatus is not None:
            ret_code = 128 + int(child.signalstatus)
    except pexpect.TIMEOUT:
        log.debug("    Subprocess %s (%d) timeout, killing" % (plugin, pp_id))
        log.warning("Test (%s) timed out after %d seconds" % (plugin, plugin_timeout))
        plugin_summary = 'UNKNOWN - test timed out after %d seconds' % plugin_timeout
        tmp_fo.write(u"4\nUNKNOWN - test timed out after %d seconds" % plugin_timeout)
        if child.isalive():
            child.terminate(force=True)
        ret_code = 3
    except Exception as e:
        log.debug("    Subprocess %s (%d) failed with generic error %s" % (plugin, pp_id, str(e)))
        plugin_summary = "UNKNOWN - test failed to execute (exception was %s)" % str(e)
        tmp_fo.write(u"4\nUNKNOWN - test failed to execute (exception was %s)" % str(e))
        ret_code = 3
        traceback.print_exc(file=sys.stdout)

    # Post-processing
    tmp_fo.close()
    log.debug("    Subprocess %d done" % pp_id)
    timestamp = time.time()
    if config['plugin_prefix'] or config['plugin_suffix']:
        log.info("[%s] SERVICE CHECK: %d (%d/%d) %s" % (str(timestamp), ret_code, os.getpid(), pp_id,
                                                        config['plugin_prefix']+plugin+config['plugin_suffix']))
    else:
        log.info("[%s] SERVICE CHECK: %d (%d/%d) %s" % (str(timestamp), ret_code, os.getpid(), pp_id, plugin))

    if config['legacy_plugins'] and plugin in config['legacy_plugins']:
        plugin_summary = '{0}'.format(status(ret_code))

    # Backend
    log.debug("    Loading backend %s (%d)" % (backend, pp_id))
    try:
        if static_backend:
            # auto-discovery is disabled as static compilation will fail on pkgutil.iter_modules()
            mod = BACKENDS_MAP[backend]
        else:
            default_pkg = os.path.dirname(wnfm.backends.__file__)
            # change to importlib.import_module for 2.7+
            if backend in [name for _, name, _ in pkgutil.iter_modules([default_pkg])]:
                mod = __import__("wnfm.backends.%s" % backend, globals(), locals(), [backend])
            else:
                log.error("Configured backend not found")
                return False
    except ImportError as e:
        log.error("Exception %s while loading backend %s" % (e, backend))
        os.remove(plugin_tmp_file)
        return True

    try:
        log.debug("    Calling backend: %s" % backend)
        signal.signal(signal.SIGALRM, _handle_timeout)
        signal.alarm(backend_timeout)
        mod.run(plugin, ret_code, plugin_tmp_file, pp_id, timestamp, config, summary=plugin_summary)
    except Exception as e:
        log.error("Exception was thrown while calling backend %s (%s)" % (backend, e))
        return True
    finally:
        signal.alarm(0)
        os.remove(plugin_tmp_file)
    return ret_code


def wn_fm_pool(plugin_dir, out_dir, backend, plugin_timeout=600, pool_size=2,
               prefix=None, suffix=None, backend_timeout=120, extra=None, service_uri=None,
               static_backend=False, fail_fast=False):
    """
    Searches for tests/plugins in the plugin directory, runs them in parallel using a process pool,
    stores results in output directory and calls configured backends to process the
    output directory further. Pool size is configurable and defaults to 2 to ensure
    all plugins will run even if one of them gets blocked. Timeouts for plugin
    and backend execution are supported. Plugin execution relies on the availability of
    pexpect module.

    :param plugin_dir:  Directory containing plugins to execute
    :param out_dir: Directory to store plugin results (output)
    :param backend: Backend to call after executing plugin
    :param plugin_timeout: Plugin execution timeout (default is 5 minutes)
    :param backend_timeout: Backend execution timeout (default is 2 minutes)
    :param pool_size: Number of plugins to run in parallel (controls size of the process pool, defaults to 2)
    :param prefix: Prefix to add to each plugin name
    :param suffix: Suffix to add to each plugin name
    :param extra: Any extra configuration that will be passed to backend
    :param service_uri: Service uri identifying the originating host (host[:port]/[resource])
    :param static_backend: Disable auto-discovery of backends (only default backends will work)
    :param fail_fast: Stop pool if one of the probes returns a non-zero code
    :return: once the pool can join
    """
    # setup
    global config
    config = dict()
    abs_out_dir = os.path.abspath(out_dir)
    abs_plugin_dir = os.path.abspath(plugin_dir)
    config['plugin_dir'] = abs_plugin_dir
    config['output_dir'] = abs_out_dir
    config['plugin_timeout'] = plugin_timeout
    config['backend'] = backend
    config['backend_timeout'] = backend_timeout
    config['plugin_prefix'] = prefix
    config['plugin_suffix'] = suffix
    config['extra'] = extra
    config['hostname'] = str(socket.gethostname()).lower()
    config['service_uri'] = service_uri
    config['static_backend'] = static_backend

    if static_backend and backend not in BACKENDS_MAP.keys():
        print('Backend {0} not supported in static mode.'.format(backend))
        sys.exit(-1)

    header = list()
    header.append("ETF WN micro Framework v.%s: %s" % (wnfm.__version__, datetime.datetime.utcnow()))
    header.append("Tests directory: %s" % abs_plugin_dir)
    header.append("Output directory: %s" % abs_out_dir)
    header.append("Python version: %s " % sys.version[0:6])
    header.append("Environment: ")
    for k in os.environ.keys():
        if 'ETF' in k:
            if ',' in os.environ[k]:
                header.append("    {0} = ".format(k))
                for e in os.environ[k].split(','):
                    header.append("          {0}".format(e))
            else:
                header.append("    {0} = {1}".format(k, os.environ[k]))
    l_max = len(max(header, key=lambda x: len(x)))
    log.info('*' * (l_max + 4))
    for l in header:
        log.info('* {0:<{1}s} *'.format(l, l_max))
    log.info('*' * (l_max + 4))

    # sanity
    if not os.path.isdir(abs_out_dir):
        # attempt to create it
        try:
            os.makedirs(abs_out_dir)
        except os.error as e:
            log.error("Unable to find/create output directory, exception was %s" % str(e))
            sys.exit(-1)

    if not os.access(abs_out_dir, os.W_OK):
        log.error("Output directory (%s) is not writable")
        sys.exit(-1)

    if not os.access(abs_plugin_dir, os.R_OK):
        log.error("Test directory (%s) is not readable")

    # pool
    log.info("Found tests: ")
    init_plugins = None
    if 'ETF_INIT' in os.environ.keys():
        init_plugins_env = os.environ['ETF_INIT'].split(',')
        init_plugins = [f for f in init_plugins_env if os.path.isfile(f) and os.access(f, os.X_OK)]
    if 'ETF_TESTS' in os.environ.keys():
        plugins_env = os.environ['ETF_TESTS'].split(',')
        plugins = [f for f in plugins_env if os.path.isfile(f) and os.access(f, os.X_OK)]
    else:
        plugins = [f for f in glob.glob(abs_plugin_dir + "/*")
                   if os.path.isfile(f) and os.access(f, os.X_OK)]
    for plugin in plugins:
        log.info("    %s" % plugin)

    # support for legacy plugins (SFT)
    if 'ETF_LEGACY' in os.environ.keys():
        legacy_plugins = os.environ['ETF_LEGACY'].split(',')
        config['legacy_plugins'] = legacy_plugins
    else:
        config['legacy_plugins'] = None

    if not plugins and not init_plugins:
        log.error("No tests found to run in %s or via env variables ETF_TESTS/ETF_INIT" % abs_plugin_dir)
        sys.exit(-1)

    if init_plugins:
        _mp_pool(init_plugins, 1, fail_fast=fail_fast)

    _mp_pool(plugins, pool_size, fail_fast=False)


def _mp_pool(plugins, pool_size, fail_fast=False):
    pool = multiprocessing.Pool(processes=pool_size)

    def sigterm_handler(signum, frame):
        log.info("Received SIGTERM, terminating the pool")
        pool.terminate()
        pool.join()
        sys.exit(137)

    signal.signal(signal.SIGTERM, sigterm_handler)
    ret_codes = pool.map(mp_worker, plugins)
    if fail_fast and any(i != 0 for i in ret_codes):
        log.info("One of the tests has returned a non-zero code and fail fast was requested, terminating.")
        pool.terminate()
        pool.join()
        sys.exit(1)
    pool.close()
    pool.join()


