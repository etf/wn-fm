import logging
import time
import datetime
import wnfm.core

log = logging.getLogger("wnfm")


def run(plugin, ret_code, out_file, pid, timestamp, config, summary=None):
    timestamp_f = datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%SZ')
    log.info("   --> backend call: %s %s:%s (%s)" % (timestamp_f, wnfm.core.status(ret_code), plugin, pid))
    time.sleep(10)
