#!/bin/sh
##############################################################################
#
# NAME:        etf_dummy.sh
#
# FACILITY:    Experiments Test Framework (ETF) - part of WLCG SAM (Service Availability Monitoring)
#
# DESCRIPTION:
#         WN executable for zero payload test jobs
#
# AUTHORS:     Marian Babik, CERN
#
##############################################################################

echo "That's all Folks"