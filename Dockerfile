FROM centos:7

#ENV CC=/opt/rh/devtoolset-7/root/usr/bin/gcc

RUN yum -y install centos-release-scl
RUN yum -y install gcc glibc glibc-common xinetd gd gd-devel make net-snmp \
                   openssl-devel unzip wget patch perl-ExtUtils-Embed \
                   perl-devel devtoolset-7 rrdtool-devel rrdtool boost boost-devel git python3 python3-pip epel-release
RUN yum -y install python3-devel chrpath pexpect
RUN git clone https://gitlab.cern.ch/etf/wn-fm.git
RUN pip3 install nuitka pexpect
RUN cd wn-fm && export PYTHONPATH=. && source scl_source enable devtoolset-7 && nuitka3  --follow-imports --python-flag=no_site --standalone bin/etf_wnfm --show-progress --show-modules
RUN cd wn-fm && ls -al &&  tar -cvzf etfwn_dist.tar.gz etf_wnfm.dist
#RUN cd static-python && git checkout 2.7 && make -f Static.make

