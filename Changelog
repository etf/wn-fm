* Wed Nov 23 2022 Marian Babik <marian.babik@cern.ch> 0.1.24
- added support for pre-stage/init plugins (ETF_INIT)
- new argument to stop execution if any of the init probes fail
- export of ETF output directory (prep for apptainer)
- preserve order of probes as defined in env.

* Tue Apr 05 2022 Marian Babik <marian.babik@cern.ch> 0.1.23
- EL9 build support 

* Mon Mar 07 2022 Marian Babik <marian.babik@cern.ch> 0.1.22
- added python binary lookup in etf_run.sh
- added cs8/cs9 CI testing (slc6, cc7 are still supported)

* Mon Oct 05 2020 Marian Babik <marian.babik@cern.ch> 0.1.21
- unicode support for pexpect 

* Mon Sep 28 2020 Marian Babik <marian.babik@cern.ch> 0.1.20
- extended timeout to wait for pid

* Mon Sep 21 2020 Marian Babik <marian.babik@cern.ch> 0.1.19
- utc time, env format

* Mon Sep 21 2020 Marian Babik <marian.babik@cern.ch> 0.1.18
- support for disabling discovery of backends (needed for statically compiled version)

* Tue Sep 08 2020 Marian Babik <marian.babik@cern.ch> 0.1.17
- auto-detect SAME return codes; removed status_map
- redirect python version to stdout

* Mon Jul 27 2020 Marian Babik <marian.babik@cern.ch> 0.1.16
- simplified summary

* Wed Jul 22 2020 Marian Babik <marian.babik@cern.ch> 0.1.15
- added support for legacy tests (SFT)

* Wed Jul 15 2020 Marian Babik <marian.babik@cern.ch> 0.1.14
- updated README.md
- support for statically compiled ETF WN-qFM
- added docker build for static compile 

* Fri Jun 19 2020 Marian Babik <marian.babik@cern.ch> 0.1.13
- added additional debugging info

* Wed Jun 17 2020 Marian Babik <marian.babik@cern.ch> 0.1.12
- pid file timeout extended

* Wed Jun 17 2020 Marian Babik <marian.babik@cern.ch> 0.1.11
- fixed setup of directories
- removed hostname from summary

* Tue Jun 16 2020 Marian Babik <marian.babik@cern.ch> 0.1.10
- added c8 and sl6 testing
- py2.6 compatiblity fixes

* Fri Jun 12 2020 Marian Babik <marian.babik@cern.ch> 0.1.9
- fixed _on_exit

* Fri Jun 12 2020 Marian Babik <marian.babik@cern.ch> 0.1.8
- py3 compat, py2.6 compat
- variables refactoring
- added ability to run in background (pid file + sigterm handler)
- added global and per process timeouts
- fixed getopts; env export; service uri
- improved plugin output processing
- fixed ret_code handling and status map

* Tue Jun 09 2020 Marian Babik <marian.babik@cern.ch> 0.1.7
- added support for service URI
- fixed mode for bin files

* Tue Jun 09 2020 Marian Babik <marian.babik@cern.ch> 0.1.6
- unit tests fixes

* Tue Jun 09 2020 Marian Babik <marian.babik@cern.ch> 0.1.5
- rpm build

* Tue Jun 09 2020 Marian Babik <marian.babik@cern.ch> 0.1.4
- added bin and pexpect support    
- improved jsonify backend
- added support for custom status map
- test configuration from env
- spec init

* Tue Nov 29 2016 Marian Babik <marian.babik@cern.ch> 0.1.2
- initial build
- added gitlab-ci
